# Desktop backgrounds in various formats but mostly jpg and png.

## Images are archived in tar.gz format.

There will be multiple files.

Filenames will indicate, in a general way, the type or topic of the images contained in the archive.
(example:  pictures-moonrocks.tar.gz)

## Disclaimer
-  At this point in time, to my knowledge, all images are unincumbered by copy rights.
